package net.betterverse.simpleconfiguration;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import net.betterverse.simpleconfiguration.serialization.SerializationException;
import net.betterverse.simpleconfiguration.serialization.SerializationManager;
import net.betterverse.simpleconfiguration.serialization.Serializer;

import org.bukkit.configuration.ConfigurationSection;


public class LoadingImpl {

    @SuppressWarnings("unchecked")
    public static <T extends EasyConfiguration> T newInstance(Class<? extends T> clazz, ConfigurationSection c) {
        try {
            Constructor<T> constructor = (Constructor<T>) clazz.getConstructor(ConfigurationSection.class);
            return constructor.newInstance(c);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
    public static void save(Object obj, ConfigurationSection config) {
        for (Field field : getFieldsWithAnnotation(obj, Load.class)) {
            handleFieldSerialization(field, config, obj);
        }
    }

    public static void inheritFrom(Object obj1, Object obj2) {
        for (Field field : getFieldsWithAnnotation(obj1, Inherit.class)) {
            try {
                if (field.get(obj1) == null) {
                    Field field2 = obj2.getClass().getField(field.getName());
                    field2.setAccessible(true);
                    if (field2.get(obj2) != null)
                        field.set(obj1, field2.get(obj2));
                }
            } catch (SecurityException e) {
            } catch (NoSuchFieldException e) {
            } catch (IllegalAccessException e) {
            }
        }
    }

    public static void sync(EasyConfiguration obj, ConfigurationSection section) {
        for (Field f : getFieldsWithAnnotation(obj, Load.class)) {
            try {
                if (!f.get(obj).equals(obj.getLoadValues().getLoadValue(f))) {
                    // To the file!
                    handleFieldSerialization(f, section, obj);
                    return;
                }
                // else
                handleFieldDeserialization(f, section, obj);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
    public static void inherit(EasyConfiguration obj1) throws InvalidConfigurationException {
        for (Field field : getFieldsWithAnnotation(obj1, Inherit.class)) {
            String path = field.getAnnotation(Inherit.class).value();
            if (path.equals("")) {
                throw new DeveloperScrewedSomethingUpException("The field: \'"+field.getName()+"\' in class: \'"+obj1.getClass().getName()+"\' doesn't have a value assigned to its Inherit Annotation.");
            }
            ConfigurationSection c = obj1.getConfig().getParent().getConfigurationSection(path);
            if (c == null){
                throw new InvalidConfigurationException("Unable to inherit the value: \'"+ field.getName() + "\' from the section: \'"+obj1.getConfig().getCurrentPath()+"."+path+"\' as the section doesn't exist.");
            }
            try {
                if (field.get(obj1) == null) {
                    field.set(obj1, c.get(field.getName()));
                }
            } catch (IllegalArgumentException e) {
                //Shouldn't happen
            } catch (IllegalAccessException e) {
                //Shouldn't happen
            }
        }
    }
    public static void inherit(Object obj1, ConfigurationSection section) {
        for (Field field : getFieldsWithAnnotation(obj1, Inherit.class)) {
            try {
                if (field.get(obj1) == null) {
                    field.set(obj1, section.get(field.getName()));
                }
            } catch (IllegalArgumentException e) {
                //Shouldn't happen
            } catch (IllegalAccessException e) {
                //Shouldn't happen
            }
        }
    }

    public static List<Field> getFieldsWithAnnotation(Object obj, Class<? extends Annotation> clazz) {
        ArrayList<Field> fields = new ArrayList<Field>();
        for (Field field : obj.getClass().getDeclaredFields()) {
            try {
                field.setAccessible(true);
                if (field.isAnnotationPresent(clazz)) {
                    fields.add(field);
                }
            } catch (SecurityException e) {

            }
        }
        return fields;
    }
    public static void handleFieldDeserialization(Field f, ConfigurationSection c, Object obj) {
        Class<?> clazz= f.getType();
        //If it's serializable and config contains a value.
        try {
            LoadData data = new LoadData(f.getAnnotation(Load.class));
            if (data.getPath().equals("")) {
                data.setPath(f.getName());
            }

            if (SerializationManager.getInstance().canSerialize(clazz) ) { //Deserialize it.
                Object value = SerializationManager.getInstance().deserialize(clazz, c, data);
                if (value != null) {
                    f.set(obj, value);
                }
                return;

            }
            else if (f.getType().isPrimitive()) {
                if (f.getType() == Boolean.TYPE) {
                    f.setBoolean(obj, c.getBoolean(f.getName()));
                }
                else if (f.getType() == Integer.TYPE) {
                    f.setInt(obj, c.getInt(f.getName()));
                }
                else if (f.getType() == Float.TYPE) {
                    f.setFloat(obj, (float)c.getDouble(f.getName()));
                }
                else if (f.getType() == Double.TYPE) {
                    f.setDouble(obj, c.getDouble(f.getName()));
                }
                else if (f.getType() == Byte.TYPE) {
                    f.setInt(f.getName(), c.getInt(f.getName()));
                }
                else if (f.getType() == Short.TYPE) {
                    f.setInt(obj, c.getInt(f.getName()));
                }
                else if (f.getType() == Long.TYPE) {
                    f.setLong(obj, c.getLong(f.getName()));
                }
                return;
            }
        } catch (IllegalArgumentException e) {
            throw new SerializationException(e);
        } catch (IllegalAccessException e) {
            throw new SerializationException(e);
        }

        throw new SerializationException("No valid serializer found for \'"+clazz.getName() +"\'.");
    }
    public static void handleFieldSerialization(Field f, ConfigurationSection c, Object obj) {
        Class<?> clazz = f.getType();
        LoadData data = new LoadData(f.getAnnotation(Load.class));
        if (data.getPath().equals("")) {
            data.setPath(f.getName());
        }
        if (SerializationManager.getInstance().canSerialize(clazz)) {
            try {
                SerializationManager.getInstance().serialize(clazz, c, f.get(obj), data);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            return;
        }
        if (f.getType().isPrimitive()) {
            try{
                if (f.getType() == Boolean.TYPE) {
                    c.set(data.getPath(), f.getBoolean(obj));
                }
                else if (f.getType() == Integer.TYPE) {

                    c.set(data.getPath(), f.getInt(obj));

                }
                else if (f.getType() == Float.TYPE) {

                    c.set(data.getPath(), f.getFloat(obj));

                }
                else if (f.getType() == Double.TYPE) {

                    c.set(data.getPath(), f.getDouble(obj));

                }
                else if (f.getType() == Byte.TYPE) {

                    c.set(data.getPath(), f.getByte(obj));

                }
                else if (f.getType() == Short.TYPE) {

                    c.set(data.getPath(), f.getShort(obj));

                }
                else if (f.getType() == Long.TYPE) {

                    c.set(data.getPath(), f.getLong(obj));

                }
                return;
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch(IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        throw new SerializationException("Unable to serialize \'"+clazz.getName()+"\'.");
    }

    public static void load(Object obj, ConfigurationSection config) {
        if (config == null) {
            return;
        }
        for (Field field : getFieldsWithAnnotation(obj, Load.class)) {
            handleFieldDeserialization(field, config, obj);
        }
    }

    public static ConfigurationSection getConfigurationSection(ConfigurationSection c, String path) {
        ConfigurationSection section = c.getConfigurationSection(path);
        if (section == null) {
            section = c.createSection(path);
        }
        return section;
    }

    public static boolean areAllFieldsSet(Object obj) {
        for (Field f : obj.getClass().getDeclaredFields()) {
            try {
                f.setAccessible(true);
                if (f.get(obj) == null) {
                    return false;
                }
            } catch (IllegalAccessException ex) {
                //Shouldn't happen
            }
        }
        return true;
    }

    public static void outputAllFields(Object obj) {
        for (Field f : obj.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            try {
                System.out.println(f.getName() + " : "+f.get(obj));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }

}
