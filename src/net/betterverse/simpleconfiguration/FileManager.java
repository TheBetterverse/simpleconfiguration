package net.betterverse.simpleconfiguration;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class FileManager {
    Map<File, FileConfiguration> configs = new HashMap<File, FileConfiguration>();

    File root;


    public FileManager(File root) {
        this.root = root;
    }

    public FileConfiguration loadConfig(File f) {
        FileConfiguration config = YamlConfiguration.loadConfiguration(f);
        configs.put(f, config);
        return config;
    }

    public FileConfiguration loadConfig(String name) {
        File file = new File(root, name+".yml");
        return loadConfig(file);
    }
    
    public void remove(File file) {
        configs.remove(file);
    }
    
    public void remove(String file) {
        File f = new File(root, file+".yml");
        remove(f);
    }

    public void save() throws IOException {
        for (Entry<File, FileConfiguration> entry : configs.entrySet()) {
            entry.getValue().save(entry.getKey());

        }
    }
    
    public void clear() {
        configs.clear();
    }

}
