package net.betterverse.simpleconfiguration;

@SuppressWarnings("serial")
public class DeveloperScrewedSomethingUpException extends RuntimeException {

    public DeveloperScrewedSomethingUpException() {
    }

    public DeveloperScrewedSomethingUpException(String msg) {
        super(msg);
    }
}
