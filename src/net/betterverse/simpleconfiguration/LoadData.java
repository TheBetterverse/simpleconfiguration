package net.betterverse.simpleconfiguration;

import net.betterverse.simpleconfiguration.serialization.Serializer;

public class LoadData {
    
    private Class<?> extra;
    
    private String path;
    
    private Class<? extends Serializer> serializer;
    
    private Class<? extends Serializer> subSerializer;
    
    private Class<?> implementation;
    
    public LoadData(Load load) {
        this(load.value(), load.path(), load.serializer(), load.subSerializer(), load.implementation());
    }
    
    public LoadData(String path, Class<?> extra) {
        this(extra, path, Serializer.class, Serializer.class, Load.class);
    }
    
    public LoadData(String path) {
        this(Load.class, path, Serializer.class, Serializer.class, Load.class);
    }
    
    public LoadData(Class<?> extra, String path, Class<? extends Serializer> alternativeSerializer, Class<? extends Serializer> subSerializer, Class<?> implementation) {
        this.extra = extra;
        this.path = path;
        this.serializer = alternativeSerializer;
        this.subSerializer = subSerializer;
        this.implementation = implementation;
    }

    public Class<?> getExtra() {
        return extra;
    }

    public void setExtra(Class<?> extra) {
        this.extra = extra;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Class<? extends Serializer> getSerializer() {
        return serializer;
    }
    
    public Class<?> getImplementation() {
        return implementation;
    }

    public void setSerializer(Class<? extends Serializer> alternativeSerializer) {
        this.serializer = alternativeSerializer;
    }

    public Class<? extends Serializer> getSubSerializer() {
        return subSerializer;
    }

    public void setSubSerializer(Class<? extends Serializer> subSerializer) {
        this.subSerializer = subSerializer;
    }
    
    public void setImplementation(Class<?> implementation) {
        this.implementation = implementation;
    }
    
    public boolean isExtraDefault() {
        return extra == Load.class;
    }
    
    public boolean isPathDefault() {
        return path.equals("");
    }
    
    public boolean isSubserializerDefault() {
        return subSerializer == Serializer.class;
    }
    
    public boolean isSerializerDefault() {
        return serializer == Serializer.class;
    }
    
    public boolean isImplementaitonDefault() {
        return implementation == Load.class;
    }


    
    

}
