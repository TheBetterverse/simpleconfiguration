/**
 * 
 */
package net.betterverse.simpleconfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.betterverse.simpleconfiguration.serialization.SerializationManager;
import net.betterverse.simpleconfiguration.serialization.Serializer;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;


public class EasyManager<T extends EasyConfiguration> implements Iterable<T> {
    Map<String, T> map = Collections.synchronizedMap(new HashMap<String, T>());
    Class<T> clazz;
    ConfigurationSection config;

    public EasyManager(Class<T> clazz, ConfigurationSection config) {
        this.clazz = clazz;
        load(config); 
        

    }
    public EasyManager(Class<T> clazz, File f) {
        this.clazz = clazz;
        load(f);
        
    }

    public void load(File f) {
        try {
            // Only creates the file if it doesn't already exist.
            File parent = f.getParentFile();
            if (!parent.exists()) {
                parent.mkdirs();
            }
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        load((ConfigurationSection) YamlConfiguration.loadConfiguration(f));
    }
    
    public void reload(ConfigurationSection section) {
        
    }

    @SuppressWarnings("unchecked")
    public void load(ConfigurationSection c) {
        for (String s : c.getKeys(false)) {
            map.put(s, (T)SerializationManager.getInstance().deserialize(clazz, c, new LoadData(s)));
        }
        config = c;

    }

    public void save(ConfigurationSection c) {
        for (Entry<String, T> e : map.entrySet()) {
            SerializationManager.getInstance().serialize(clazz, c, e.getValue(), new LoadData(e.getKey()));
        }
        config = c;
        
    }
    
    public ConfigurationSection getConfig() {
        return config;
    }
    
    public void save() {
        save(config);
    }

    public T get(String name) {
        return map.get(name);
    }

    public boolean contains(String name) {
        return map.containsKey(name);
    }

    public Class<T> getType() {
        return clazz;
    }

    public boolean add(String s) {
        if (map.containsKey(s)) {
            return false;
        }
        map.put(s, LoadingImpl.newInstance(clazz, LoadingImpl.getConfigurationSection(config, s)));
        return true;
    }

    // All methods from Set, added in for ease of use of the manager. Becasuse
    // the manager isn't actually a set, it doesn't implement set.
    public boolean add(T t) {
        if (map.containsKey(t.getName()))
            if (map.get(t.getName()).equals(t))
                return false;
        map.put(t.getName(), t);
        return true;
    }

    public boolean addAll(Collection<? extends T> c) {
        boolean returnVal = true;
        for (T t : c)
            if (!add(t))
                returnVal = false;
        return returnVal;

    }

    public void clear() {
        map.clear();

    }

    @SuppressWarnings("unchecked")
    public boolean contains(Object o) {
        return (o.getClass().isAssignableFrom(clazz) && map.containsKey(((T) o).getName()));

    }

    public boolean containsAll(Collection<?> c) {
        for (Object obj : c)
            if (!contains(obj))
                return false;
        return true;
    }

    public boolean isEmpty() {
        return (map.size() == 0);
    }

    public Iterator<T> iterator() {
        return map.values().iterator();
    }

    public boolean remove(Object o) {
        return (map.remove(o) != null);
    }

    public boolean removeAll(Collection<?> c) {
        boolean returnVal = true;
        for (Object t : c)
            if (!remove(t))
                returnVal = false;
        return returnVal;
    }

    public boolean retainAll(Collection<?> c) {
        boolean changed = false;
        for (Entry<String, T> entry : map.entrySet())
            if (!c.contains(entry.getValue()))
                if (map.remove(entry.getValue()) != null)
                    changed = true;
        return changed;
    }

    public int size() {
        return map.size();
    }

    public Object[] toArray() {
        return map.values().toArray();
    }

    public T[] toArray(T[] arg0) {
        return map.values().toArray(arg0);
    }

    public List<T> asList() {
        ArrayList<T> list = new ArrayList<T>();
        for (T t : this)
            list.add(t);
        return list;
    }

}
