package net.betterverse.simpleconfiguration;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class LoadValues {
    
    private final Map<String, Object> values = new HashMap<String, Object>();
    
    public LoadValues(Object obj) {
        for (Field f : LoadingImpl.getFieldsWithAnnotation(obj, Load.class)) {
            try {
                values.put(f.getName(), f.get(obj));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }
    
    public Object getLoadValue(Field f) {
        return values.get(f.getName());
    }

}
