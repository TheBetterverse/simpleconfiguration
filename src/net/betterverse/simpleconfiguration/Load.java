/**
 * 
 */
package net.betterverse.simpleconfiguration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import net.betterverse.simpleconfiguration.serialization.Serializer;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Load { 

    public Class<?> value() default Load.class;
    
    public Class<?> implementation() default Load.class;
    
    public String path() default "";
    
    public Class<? extends Serializer> serializer() default Serializer.class;
    
    public Class<? extends Serializer> subSerializer() default Serializer.class;

}
