package net.betterverse.simpleconfiguration.serialization;

import net.betterverse.simpleconfiguration.LoadData;
import net.betterverse.simpleconfiguration.LoadingImpl;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;


public class BukkitSerializer implements Serializer{

    @Override
    public void serialize(ConfigurationSection c, Object obj, LoadData data) {
        Class<?> clazz = obj.getClass();
        ConfigurationSection section = LoadingImpl.getConfigurationSection(c, data.getPath());
        if (clazz == Location.class) {
            Location l = (Location)obj;
            section.set("x", l.getX());
            section.set("y", l.getY());
            section.set("z", l.getZ());
            section.set("yaw", l.getYaw());
            section.set("pitch", l.getPitch());
            section.set("world", l.getWorld().getName());

        }
        else if (clazz == World.class) {
            c.set(data.getPath(), ((World)obj).getName());
        }

    }


    @Override
    public Object deserialize(ConfigurationSection c, Class<?> clazz, LoadData data) {
        ConfigurationSection section = LoadingImpl.getConfigurationSection(c, data.getPath());
        try {
            if (clazz == Location.class) {
                double x = section.getDouble("x");
                double y = section.getDouble("y");
                double z = section.getDouble("z");
                float pitch = (float) section.getDouble("pitch");
                float yaw = (float) section.getDouble("yaw");
                World world = Bukkit.getWorld(section.getString("world"));
                return new Location(world, x, y, z, yaw, pitch);
            }
            else if (clazz == World.class) {
                return Bukkit.getWorld(c.getString(data.getPath()));
            }
        } catch (NullPointerException e) {
            return null; // Happens if the world is null.
        }
        throw new SerializationException("BukkitSerializer is unable to serialize "+clazz);
    }

}
