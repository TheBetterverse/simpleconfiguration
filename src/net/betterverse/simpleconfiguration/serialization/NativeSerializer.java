package net.betterverse.simpleconfiguration.serialization;

import net.betterverse.simpleconfiguration.LoadData;

import org.bukkit.configuration.ConfigurationSection;

public class NativeSerializer implements Serializer {

	@Override
	public void serialize(ConfigurationSection c, Object obj, LoadData data) {
		c.set(data.getPath(), obj);
		
		
	}

	@Override
	public Object deserialize(ConfigurationSection c, Class<?> clazz, LoadData data) {
		return c.get(data.getPath());
	}

}
