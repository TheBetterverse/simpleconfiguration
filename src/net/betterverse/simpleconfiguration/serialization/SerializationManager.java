package net.betterverse.simpleconfiguration.serialization;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.betterverse.simpleconfiguration.EasyConfiguration;
import net.betterverse.simpleconfiguration.LoadData;
import net.betterverse.simpleconfiguration.LoadingImpl;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;


public class SerializationManager {

    static SerializationManager i;

    static {
        i = new SerializationManager();
        i.registerSerializer(new NativeSerializer(), String.class);
        i.registerSerializer(new MapSerializer(), Map.class);
        i.registerSerializer(new EnumSerializer(), Enum.class);
        i.registerSerializer(new ListSerializer(), List.class);
        i.registerSerializer(new SetSerializer(), Set.class);
        BukkitSerializer bSerializer = new BukkitSerializer();
        i.registerSerializer(bSerializer, Location.class);
        i.registerSerializer(bSerializer, World.class);
    }
    public static SerializationManager getInstance() {
        return i;
    }
    private HashMap<Class<?>, Serializer> serializers = new HashMap<Class<?>, Serializer>();
    private List<Serializer> serializerObjects = new ArrayList<Serializer>();

    public void registerSerializer(Serializer serializer, Class<?> clazz) {
        serializers.put(clazz, serializer);
        serializerObjects.add(serializer);
    }
    
    public void addAlternativeSerializer(Serializer serializer) {
        serializerObjects.add(serializer);
    }
    
    @SuppressWarnings("unchecked")
    public Object deserialize(Class<?> clazz, ConfigurationSection c, LoadData data) {
        if (data.getSerializer() != Serializer.class) {
            for (Serializer s : serializerObjects) {
                if (s.getClass() == data.getSerializer()) {
                    return s.deserialize(c, clazz, data); 
                }
            }
        }
        if (EasyConfiguration.class.isAssignableFrom(clazz)) {
            return LoadingImpl.newInstance((Class<? extends EasyConfiguration>)clazz, LoadingImpl.getConfigurationSection(c, data.getPath()));
        } 
        if (serializers.containsKey(clazz)) {
            return serializers.get(clazz).deserialize(c, clazz, data);
        }
        for (Entry<Class<?>, Serializer> entry : serializers.entrySet()) {
            if (entry.getKey().isAssignableFrom(clazz)) {
                serializers.put(clazz, entry.getValue());
                return entry.getValue().deserialize(c, clazz, data);
            }
        }
        throw new SerializationException("No valid serializer found for \'"+clazz.getName() +"\'.");
    }
    
    public void serialize(Class<?> clazz, ConfigurationSection c,  Object obj, LoadData data) {
        if (clazz == Serializer.class) {
            throw new SerializationException("An object which requires an extra value was not given one.  Unable to serialize.");
        }
        if (data.getSerializer() != Serializer.class) {
            for (Serializer s : serializerObjects) {
                if (s.getClass() == data.getSerializer()) {
                    s.serialize(c, obj, data); 
                    return;
                }
            }
        }
        if (obj instanceof EasyConfiguration) {
            ((EasyConfiguration)obj).save(LoadingImpl.getConfigurationSection(c, data.getPath()));
            return;
        }
        if (obj == null) {
            c.set(data.getPath(), null);
            return;
        }
        if (serializers.containsKey(clazz)) {
            serializers.get(clazz).serialize(c, obj, data);
            return;
        }
        for (Entry<Class<?>, Serializer> entry : serializers.entrySet()) {
            if (entry.getKey().isAssignableFrom(clazz)) {
                serializers.put(clazz, entry.getValue());
                entry.getValue().serialize(c, obj, data);
                return;
            }
        }
        throw new SerializationException("No valid serializer found for \'"+clazz.getName() +"\'.");
    }
    public boolean canSerialize(Class<?> clazz) {
        if (clazz.isPrimitive()){
            return false;
        }
        if (clazz.isInstance(EasyConfiguration.class)) {
            return true;
        }
        if (serializers.containsKey(clazz)) {
            return true;
        }
        for (Entry<Class<?>, Serializer> entry : serializers.entrySet()) {
            if (entry.getKey().isAssignableFrom(clazz)) {
                serializers.put(clazz, entry.getValue());
                return true;
            }
        }
        return false;
    }


}
