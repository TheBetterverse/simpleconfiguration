package net.betterverse.simpleconfiguration.serialization;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.betterverse.simpleconfiguration.LoadData;

import org.bukkit.configuration.ConfigurationSection;

public class SetSerializer implements Serializer{

    @Override
    public void serialize(ConfigurationSection c, Object obj, LoadData data) {
        Set<?> set = (Set<?>)obj;
        List<String> values = new ArrayList<String>();
        for (Object o : set) {
            values.add(o.toString());
        }
        c.set(data.getPath(), values);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public Object deserialize(ConfigurationSection c, Class<?> clazz, LoadData data) {
        if (clazz == Set.class && !data.isImplementaitonDefault()) {
            clazz = data.getImplementation();
        }
        if (clazz == Set.class) {
            clazz = HashSet.class;
        }
        List<String> stringList = c.getStringList(data.getPath());
        Set<Object> list = instantiateType(clazz);
        if (data.getExtra() == String.class) {
            for (int i = 0; i < stringList.size(); i++) {
                list.add(stringList.get(i));
            }
        }
        else if (data.getExtra().isInstance(Enum.class)) {
            for (int i = 0; i < stringList.size(); i++) {
                list.add(Enum.valueOf((Class<? extends Enum>)data.getExtra(), stringList.get(i)));
            }
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    public Set<Object> instantiateType(Class<?> clazz) {
        try {
            return (Set<Object>) clazz.getDeclaredConstructor().newInstance();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

}
