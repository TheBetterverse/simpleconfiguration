package net.betterverse.simpleconfiguration.serialization;

import java.util.HashMap;
import java.util.Map;

import net.betterverse.simpleconfiguration.LoadData;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;

public class ConfigurationSerializableSerializer implements Serializer{

    @Override
    public void serialize(ConfigurationSection c, Object obj, LoadData data) {
        c.set(data.getPath(), ((ConfigurationSerializable)obj).serialize());
    }

    @SuppressWarnings("unchecked")
    @Override
    public Object deserialize(ConfigurationSection c, Class<?> clazz, LoadData data) {
        Map<String, Object> map = new HashMap<String, Object>();
        ConfigurationSection section = c.getConfigurationSection(data.getPath());
        if (section == null) {
            return null;
        }
        for (String s : section.getKeys(true)) {
            map.put(s, section.get(s));
        }
        return ConfigurationSerialization.deserializeObject(map, (Class<? extends ConfigurationSerializable>) clazz);
    }

}
