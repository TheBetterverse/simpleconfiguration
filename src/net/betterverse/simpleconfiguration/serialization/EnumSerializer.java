package net.betterverse.simpleconfiguration.serialization;

import net.betterverse.simpleconfiguration.LoadData;

import org.bukkit.configuration.ConfigurationSection;

public class EnumSerializer implements Serializer{

    @Override
    public void serialize(ConfigurationSection c, Object obj, LoadData data) {
        c.set(data.getPath(), ((Enum<?>)obj).name());
        
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public Object deserialize(ConfigurationSection c, Class<?> clazz, LoadData data) {
        String enumValue = c.getString(data.getPath());
        return Enum.valueOf((Class<? extends Enum>)clazz, enumValue);
    }

}
