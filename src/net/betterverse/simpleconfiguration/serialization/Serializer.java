package net.betterverse.simpleconfiguration.serialization;

import net.betterverse.simpleconfiguration.LoadData;

import org.bukkit.configuration.ConfigurationSection;

public interface Serializer {
	
	public void serialize(ConfigurationSection c, Object obj, LoadData data);
	
	public Object deserialize(ConfigurationSection c, Class<?> clazz, LoadData data);

}
