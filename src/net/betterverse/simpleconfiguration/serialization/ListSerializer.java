package net.betterverse.simpleconfiguration.serialization;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import net.betterverse.simpleconfiguration.LoadData;
import net.betterverse.simpleconfiguration.LoadingImpl;

import org.bukkit.configuration.ConfigurationSection;

public class ListSerializer implements Serializer {

    @Override
    public void serialize(ConfigurationSection c, Object obj, LoadData data) {
        ConfigurationSection targetSection = LoadingImpl.getConfigurationSection(c, data.getPath());
        List<?> list = (List<?>)obj;
        for (int i = 0; i < list.size(); i++) {
            SerializationManager.getInstance().serialize(data.getExtra(), targetSection, list.get(i), new LoadData(Integer.toString(i)));
        }

    }

    @Override
    public Object deserialize(ConfigurationSection c, Class<?> clazz, LoadData data) {
        ConfigurationSection targetSection = LoadingImpl.getConfigurationSection(c, data.getPath());
        if (clazz == List.class && !data.isImplementaitonDefault()) {
            clazz = data.getImplementation();
        }
        if (clazz == List.class) {
            clazz = ArrayList.class;
        }
        Set<String> values = targetSection.getKeys(false);
        List<Object> list = instantiateType(clazz);
        for (int i = 0; i < values.size(); i++) {
            list.add(SerializationManager.getInstance().deserialize(data.getExtra(), targetSection, new LoadData(Integer.toString(i))));
        }
        return list;
    }
    
    @SuppressWarnings("unchecked")
    public List<Object> instantiateType(Class<?> clazz) {
        try {
            return (List<Object>) clazz.getDeclaredConstructor().newInstance();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

}
