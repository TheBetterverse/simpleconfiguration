package net.betterverse.simpleconfiguration.serialization;

@SuppressWarnings("serial")
public class SerializationException extends RuntimeException{

	public SerializationException(String msg) {
		super(msg);
	}
	public SerializationException(Exception e) {
		super(e);
	}
}
