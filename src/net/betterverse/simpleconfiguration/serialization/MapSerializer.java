package net.betterverse.simpleconfiguration.serialization;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import net.betterverse.simpleconfiguration.LoadData;
import net.betterverse.simpleconfiguration.LoadingImpl;

import org.bukkit.configuration.ConfigurationSection;

public class MapSerializer implements Serializer{

    @SuppressWarnings("unchecked")
    @Override
    public void serialize(ConfigurationSection c, Object obj, LoadData data) {
        ConfigurationSection section = LoadingImpl.getConfigurationSection(c, data.getPath());
        Map<String,?> map = (Map<String,?>)obj;
        for (Entry<String, ?> entry : map.entrySet()) {
            SerializationManager.getInstance().serialize(data.getSubSerializer(), section, entry.getValue(), new LoadData(entry.getKey()));
        }
    }

    @Override
    public Object deserialize(ConfigurationSection c, Class<?> clazz, LoadData data) {
        ConfigurationSection section = LoadingImpl.getConfigurationSection(c, data.getPath());
        Map<String, Object> map = new HashMap<String, Object>();
        for (String s : section.getKeys(false)) {
           map.put(s, SerializationManager.getInstance().deserialize(data.getExtra(), section, new LoadData(s, data.getSerializer())));
        }
        return map;
    }

}
