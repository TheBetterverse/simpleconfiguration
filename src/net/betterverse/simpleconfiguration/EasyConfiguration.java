/**
 * 
 */
package net.betterverse.simpleconfiguration;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * @author Nate Mortensen
 * 
 */
public abstract class EasyConfiguration implements Comparable<String> {
    private ConfigurationSection config;
    protected String name;
    private LoadValues loadValues;
    
    public int compareTo(String o) {
        return name.compareTo(o);
    }

    public void load(File file) {
        try {
            // Only creates the file if it doesn't already exist.
            File parent = file.getParentFile();
            if (!parent.exists()) {
                parent.mkdirs();
            }
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        load((ConfigurationSection) YamlConfiguration.loadConfiguration(file));
    }
    
    public void setConfig(ConfigurationSection config) {
        this.config = config;
    }
    
    public void sync(ConfigurationSection config) {
        LoadingImpl.sync(this, config);
    }
    
    public LoadValues getLoadValues() {
        return loadValues;
    }

    public void load(ConfigurationSection config) {
        this.config = config;
        name = config.getName();
        LoadingImpl.load(this, config);
    }
    protected void inherit() {
        LoadingImpl.inherit(this);
    }
    protected void inherit(ConfigurationSection c) {
        LoadingImpl.inherit(this, c);
    }
    protected void inherit(Object other) {
        LoadingImpl.inheritFrom(this, other);
    }
    protected void inherit(String path) {
        ConfigurationSection c = config.getParent().getConfigurationSection(path);
        if (c == null){
            throw new InvalidConfigurationException("Unable to inherit from the section: \'"+getConfig().getCurrentPath()+"."+path+"\' as the section doesn't exist.");
        }
        inherit(c);
    }

    public void save() {
        LoadingImpl.save(this, config);
    }
    public void save(ConfigurationSection c) {
        LoadingImpl.save(this, c);
    }

    public String getName() {
        return name;
    }

    public ConfigurationSection getConfig() {
        return config;
    }
    
    public boolean areAllFieldsSet() {
        return LoadingImpl.areAllFieldsSet(this);
    }
}
